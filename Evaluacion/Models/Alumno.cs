﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Evaluacion.Models
{
    public class Alumno
    {
        [Required(ErrorMessage ="Nombre del alumno es requerido"), DisplayName("Nombre"),MaxLength(50)]
        public String Nombre { get; set; }
        [Required(ErrorMessage = "Apellido Paterno del alumno es requerido"), DisplayName("Apellido Paterno"),MaxLength(100)]
        public String APaterno { get; set; }
        [Required(ErrorMessage = "Apellido Materno del alumno es requerido"), DisplayName("Apellido Materno"), MaxLength(100)]
        public String AMaterno { get; set; }
        [DisplayName("Fecha de Naciemiento")]
        public String FNacimiento { get; set; }
        [DisplayName("Genero")]
        public String Genero { get; set; }
        [DisplayName("Fecha de ingreso")]
        public String FIngreso { get; set; }
        [DisplayName("Activo")]
        public Boolean Activo { get; set; }
        [Required(ErrorMessage = "RFC del alumno es requerido"), DisplayName("RFC"), MaxLength(13)]
        public String RFC { get; set; }
    }
}