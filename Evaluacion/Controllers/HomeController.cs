﻿using Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Evaluacion.Controllers
{
    public class HomeController : Controller
    {
        Repositorio.RepositorioAlumno repo;
        public HomeController() {
            repo = new Repositorio.RepositorioAlumno();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GuardarAlumno() 
        {
            return View();
        
        }

        [HttpPost]
        public ActionResult GuardarAlumno(Alumno alumno)
        {
            Response response;
            if (!ModelState.IsValid)
            {
                response = new Response() { httpstatuscode = 400, message = "Datos incorrrectos", data = alumno };
                return new JsonResult() { Data = response };
            }

            return new JsonResult() { Data = repo.Guardar(alumno) };
        }

    }
}