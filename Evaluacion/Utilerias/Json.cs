﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Evaluacion.Utilerias
{
    public class Json
    {
        public static string serialize(object objeto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(objeto);
        }
    }
}