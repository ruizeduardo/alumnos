﻿using Evaluacion.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;


namespace Evaluacion.Repositorio
{
    public class RepositorioAlumno
    {
        String mensaje;
        int statuscode;

        public Response Guardar(Alumno alumno)
        {
            var folder = HttpContext.Current.Server.MapPath("~/Alumnos/");
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
                string filePath = System.IO.Path.Combine(folder, String.Format("{0}.txt", alumno.RFC));

                try
                {
                    using (StreamWriter writer = File.CreateText(filePath))
                    {
                        writer.WriteLine(Utilerias.Json.serialize(alumno));
                        mensaje = "Alumno guardado con exito";
                        statuscode = 200;
                    }
                } catch (Exception ex) {
                    statuscode = 500;
                    mensaje = ex.InnerException.Message;
                }

            

            return new Response() { message= mensaje, httpstatuscode = statuscode, data=alumno };

        }
    }
}